Categories:System
License:Apache-2.0
Web Site:https://github.com/BennyKok/OpenLauncher/blob/HEAD/README.md
Source Code:https://github.com/BennyKok/OpenLauncher
Issue Tracker:https://github.com/BennyKok/OpenLauncher/issues

Auto Name:OpenLauncher
Summary:Launch applications and manage homescreen
Description:
Launcher that aims to be a powerful and community driven project.
.

Repo Type:git
Repo:https://github.com/BennyKok/OpenLauncher

Build:alpha1-patch1,2
    commit=32d793ccac3380f022feb9019ee63dc381ff6bf2
    subdir=launcher
    gradle=yes

Build:alpha1-patch2,3
    commit=c66363c5ed14974d9e416b99ff7106899850ebfc
    subdir=launcher
    gradle=yes

Build:alpha1-patch3,4
    commit=13324fc4b9f2133cc6271eb78bb52c0ce781f3e5
    subdir=launcher
    gradle=yes

Build:alpha2,5
    commit=a2180dd217b08ca544375ac0c6e650aafa65855a
    subdir=launcher
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation'\n\ndisable 'ExtraTranslation' } }" >> build.gradle

Build:alpha2_patch1,6
    commit=00b9280dfe483a1c0ec4a604c6dccc96e29e9fac
    subdir=launcher
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:alpha2_patch1
Current Version Code:6
